

# Java Fundamendals Coding

[![N|Solid](https://cldup.com/dTxpPi9lDf.thumb.png)](https://nodesource.com/products/nsolid)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

 Java Fundamendals module consisting of Advanced Java8 topics and coding exercises

  - Exercises, Gotchas
  - Warehouse exc. 
  - Developers exc.
  - Vehicle exc.
  
  - Apache Commons Basic bits 
  - Java 9 API
  - Negate predicate in lambdas Java 11 API 
  - Dates, Numbers, Formatters  
  
# To build:

  - Checkout master branch from https://gitlab.com/wsaulius/java-fundamentals 

```sh
$ git clone https://gitlab.com/wsaulius/java-fundamentals -b master 
$ cd java-fundamendals 
$ mvn clean package surefire:test surefire-report:report pmd:pmd site 

```
