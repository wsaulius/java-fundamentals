package advanced.coding.day3.gotchas;

import advanced.coding.day1.exercises.warehouse.ShoppingItem;
import advanced.coding.day1.exercises.warehouse.WareHouseLocator;
import advanced.coding.day2.exercises.vehicle.CarImpl;
import advanced.coding.day2.exercises.vehicle.Vehicle;
import advanced.coding.day2.exercises.vehicle.VehicleImpl;
import advanced.coding.day2.exercises.vehicle.VehicleType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;

public class FascinatingOptionals {

    // Initialize logger
    public static Logger log = LogManager.getLogger(FascinatingOptionals.class);

    public static void main(String[] args) {

        // Car budget is 10000 EUR
        BigDecimal myCarBudget = BigDecimal.TEN.multiply(BigDecimal.valueOf(1000D));
        final Float carAskingPrice = 9999.99f;

        Vehicle myPossibyCar = null;
        Optional<ShoppingItem> getCar = Optional.empty();

        try {
            myPossibyCar = (Vehicle) Optional.ofNullable(
                    new VehicleImpl().setPrice(carAskingPrice)
                            .getPriceOfProduct().compareTo( myCarBudget ) == -1 ?
                            new CarImpl()
                                    .setPrice(carAskingPrice)
                                    .setVehicleType(VehicleType.CAR)
                                    .setManufacturer("Toyota")
                                    .setMakeYear(LocalDate.of(2017, Month.JULY, 22))
                                    .setNameOfProduct("Tj Cruiser")
                                    .setCountOfProducts(1)
                            :
                            null ).orElseThrow( () -> new Throwable() {
                @Override
                public String toString() {
                    return "Sorry, too expensive for the budget of " + myCarBudget;
                }
            });

        } catch (Throwable throwable) {

            log.error(throwable.getMessage());
        }

        final Set<Vehicle> myCarCollection = new HashSet();

        // JAVA9 API level
        Optional.ofNullable(myPossibyCar)
                .ifPresentOrElse((car) -> {
                            System.out.println(
                                    "Car purchased is present, its: " + car);
                            myCarCollection.add(car);
                        },
                        () -> {
                            System.out.println(
                                    "No budget for the asking price " + carAskingPrice);
                        });

        myCarCollection.stream().filter(
                WareHouseLocator.nameMatches(Pattern.compile("Tj\\s+.*")))
                .map(car -> car.toString().toUpperCase()).sorted().forEach(System.out::println);

        log.debug( "CARS: " + myCarCollection.toArray() );

    }
}
