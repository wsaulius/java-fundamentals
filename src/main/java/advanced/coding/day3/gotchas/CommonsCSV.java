package advanced.coding.day3.gotchas;

import java.io.FileReader;
import java.io.Reader;
import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.opencsv.CSVReader;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

public class CommonsCSV {

        public static void main(String args[]) throws Exception {

            final FileReader sampleFileReader =  new FileReader("./data/sample.csv");

            //Instantiating the CSVReader class
            CSVReader reader = new CSVReader( new FileReader("./data/sample.csv") );
            //Reading the contents of the csv file
            StringBuffer buffer = new StringBuffer();

            String line[];
            while ((line = reader.readNext()) != null) {

                for(int i = 0; i<line.length; i++) {
                    System.out.print(line[i] + "|" );
                }

                System.out.println(" ");
            }

            final String[] HEADERS = { "author", "title", "year"};

            Reader in = sampleFileReader;
            Iterable<CSVRecord> records = CSVFormat.DEFAULT
                    .withCommentMarker( ';')
                    .withHeader(HEADERS)
                    .withFirstRecordAsHeader()
                    .parse(in);

            final List<CSVRecord> allMyBooks = new ArrayList<>();

            int idx = 1;
            for ( CSVRecord record : records ) {
                String author = record.get( HEADERS[0] );
                String title = record.get( HEADERS[1] );
                LocalDate theDate = LocalDate.of(
                        Integer.parseInt(
                        record.get( HEADERS[2] )), Month.JANUARY, 1 );

                allMyBooks.add( record );

              System.out.println( idx++ + ")Author: " + author + "\t" + "Title: " + title
                        + "\t" + "Date: " + theDate );

            }

            // Negate filter predicate : JAVA11
            System.out.println(
            allMyBooks.stream().filter( Predicate.not( CSVRecord::hasComment ) )
                    .collect(Collectors.toList() ) );
        }
    }
