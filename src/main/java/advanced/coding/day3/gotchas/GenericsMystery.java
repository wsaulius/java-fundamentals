package advanced.coding.day3.gotchas;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.*;


// Generics and instanceof
public class GenericsMystery {

    public static void main(String[] args) {

        final List<List<String>> genericJava7 = new ArrayList<List<String>>();
        final List<List<String>> genericJava8 = new ArrayList<>();

        doSomethingUseful(Collections.singleton(Arrays.asList(new Integer[]{ 10 }).toArray()));
        doSomethingUseful(Collections.singleton(Arrays.asList(new String[]{ "20" }).toArray()));
        doSomethingUseful(Collections.singleton(Arrays.asList(new NestedParameter[]{
                new NestedParameter("UsefulParam") }).toArray()));

        doSomethingUseful(Collections.singleton(Arrays.asList( genericJava7 ).toArray()));
        doSomethingUseful(Collections.singleton(Arrays.asList( genericJava8 ).toArray()));

        final List<NestedHolder<String>> nestedStrings = new ArrayList<>();
        final List<NestedHolder<Number>> nestedNumbers = new ArrayList<>();
        final List<NestedHolder<Class>> nestedClass = new ArrayList<>();

        nestedNumbers.add( new NestedHolder<>( (byte) 10) );
        nestedNumbers.add( new NestedHolder<>( 100 ) );
        nestedNumbers.add( new NestedHolder<>( 20f ) );
        nestedNumbers.add( new NestedHolder<>( new BigDecimal( BigInteger.TEN )) );

        // Even like this
        nestedClass.add( new NestedHolder<>( nestedNumbers.getClass() ));

        doSomethingUseful(Collections.singleton( nestedNumbers ));

        doSomethingUseful(Collections.singleton( nestedStrings ));

        doSomethingUseful(Collections.singleton( nestedClass ));
    }

    public static void doSomethingUseful(final Set<?> set) {
        Set<?> copy = new HashSet<Object>(set);

        synchronized ( copy ) {

            copy.stream().forEach( o -> {
                System.out.println( o.getClass().getCanonicalName() );

            if ( o instanceof Collection ) {

                ((Collection) o).stream().forEach( System.out::println );
            }

            } );
        }
    }

    public static class NestedParameter {

        protected NestedParameter(final String name ) {
            this.name = name;
        }

        protected String name;
    }

    public static class NestedHolder<T> {

        private T t;

        public NestedHolder(T t) {
            this.t = t;
        }

        public T getObject() {
            return t;
        }

        @Override
        public String toString() {
            final StringBuilder sb = new StringBuilder("NestedHolder{");
            sb.append("t=").append( this.getObject() );
            sb.append('}');
            return sb.toString();
        }
    }

}
