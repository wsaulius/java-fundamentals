package advanced.coding.day3.gotchas;

import org.apache.commons.io.IOUtils;
import org.apache.commons.text.StringEscapeUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

public class CommonsIO {

    // Initialize logger
    public static Logger log = LogManager.getLogger(CommonsIO.class);

    public static void main(String[] args) {

        InputStream in = null;
        try {
            in = new URL( "http://commons.apache.org" ).openStream();

            InputStreamReader inR = new InputStreamReader( in );
            System.out.println( StringEscapeUtils.escapeHtml3( IOUtils.toString( inR ) ) );

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            IOUtils.closeQuietly(in);
        }

    }
}
