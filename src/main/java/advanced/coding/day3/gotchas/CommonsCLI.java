package advanced.coding.day3.gotchas;

import org.apache.commons.cli.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.PrintWriter;
import java.util.Date;

/*

 Do not forget to add this to your project pom.xml

*       <!-- https://mvnrepository.com/artifact/commons-cli/commons-cli -->
        <dependency>
            <groupId>commons-cli</groupId>
            <artifactId>commons-cli</artifactId>
            <version>1.4</version>
        </dependency>
* */

public class CommonsCLI {

    // Initialize logger
    public static Logger log = LogManager.getLogger(CommonsCLI.class);

    public static void main(String[] args) {

        // create Options object
        Options options = new Options();

        // add t option
        options.addOption("t", false, "Display current time")
                .addOption("p", "print", false, "Send print request to printer.")
                .addOption("g", "gui", false, "Show GUI Application")
                .addOption("n", true, "No. of copies to print");

        final HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp( CommonsCLI.class.toString() + " demo", options );

        final PrintWriter writer = new PrintWriter(System.out);
        formatter.printUsage(writer,80,CommonsCLI.class.toString() + " demo", options);
        writer.flush();

        try {
            CommandLine commandLine = new DefaultParser().parse( options, args );

            //hasOptions checks if option is present or not
            if( commandLine.hasOption("t") ) {
                System.out.println( "Current time:" + new Date() +"\n" );

            } else if (
                    commandLine.hasOption("n") &
                    !commandLine.getOptionValue( "n" ).isEmpty() ) {

                System.out.println( String.format( "Print %d copies." ,
                       Integer.parseInt( commandLine.getOptionValue( "n" ))));
            } else

                {
                System.out.println( "No time option." );
                }

        } catch (ParseException e) {
            e.printStackTrace();
            log.error( e.getMessage() );
        }
    }
}
