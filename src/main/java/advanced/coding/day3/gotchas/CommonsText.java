package advanced.coding.day3.gotchas;

import org.apache.commons.text.StringEscapeUtils;
import org.apache.commons.text.StringSubstitutor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.apache.commons.lang3.StringUtils;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

// Placeholder subst test
public class CommonsText {

    // Initialize logger
    public static Logger log = LogManager.getLogger(CommonsText.class);

    public static void main(String[] args) {

        System.out.println(
        StringSubstitutor.replaceSystemProperties(
"You are running with java.version = ${java.version} and os.name = ${os.name}.") );

        System.out.println(
        StringEscapeUtils.escapeHtml3( String.format( "< %s & >",
                String.format("The date is %1$tm ( %<tB ) / %<te ( %<tA ) / %<tY",
                        new Date( ) )
        ) ) );

        // Build map
        Map<String, String> valuesMap = new HashMap<>();
        valuesMap.put("animal", "quick brown fox".toUpperCase());
        valuesMap.put("target", "lazy dog");
        String templateString = "The ${animal} jumped over the ${target}.";

        // Build StringSubstitutor
        final StringSubstitutor sub1 = new StringSubstitutor(valuesMap);

        // Replace
        String resolvedString = sub1.replace(templateString);
        System.out.println( "With replaced: " + resolvedString );

        valuesMap = new HashMap<>();
        final StringSubstitutor sub2  = new StringSubstitutor(valuesMap);
        valuesMap.put("animal", "big\tbrown\nmoose".toUpperCase());
        valuesMap.put("target", "fence");

        String saveMyWork = sub2.replace(templateString);

        System.out.println( "Print as many lines: " + saveMyWork );

        System.out.println(
        StringUtils.deleteWhitespace( saveMyWork ) );
    }
}
