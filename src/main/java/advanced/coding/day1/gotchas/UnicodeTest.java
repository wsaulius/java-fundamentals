package advanced.coding.day1.gotchas;

import java.nio.charset.Charset;
public class UnicodeTest {

    public static void main(String[] args) {

        // https://en.wikipedia.org/wiki/List_of_Unicode_characters
        System.out.println( String.format(
                "Math %s is %f \u00a9" , "\u03C0 ", Math.PI ) );

        String original = "\u03C0\u00a9";

        byte[] utf8Bytes = new byte[0];

        // UTF-8, otherwise it will not work
        utf8Bytes = original.getBytes();
        byte[] defaultBytes = original.getBytes();

        String roundTrip = null;

        // Gotcha! (do not alter Charset and do not rely on default one)
        roundTrip = new String( utf8Bytes, Charset.forName( "ISO-8859-1") );
        System.out.println("roundTrip = " + roundTrip);
    }
}
