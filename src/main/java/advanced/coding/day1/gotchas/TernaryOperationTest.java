package advanced.coding.day1.gotchas;

import java.math.BigInteger;

public class TernaryOperationTest {

    static byte logicCounter = 0;

    public static void main(String[] args) {

        // Bitwise AND & --  does both sides
        // Boolean AND && - "optimizes" (Gotcha!)
        //                   false && anything == false
        Boolean valueAsTernary = ( exitAsFalse() && exitAsTrue() ) ?
                // IF (true)
                Boolean.valueOf( "true" ) :
                // ELSE
                Boolean.valueOf( "false" );

        System.out.println("1#) valueAsTernary: assigned " + valueAsTernary );

        valueAsTernary = false;

        if ( true ) {
            valueAsTernary = Boolean.valueOf( "true" );
            System.out.println("2# ) valueAsTernary: " + valueAsTernary );
        } else {
            valueAsTernary = Boolean.valueOf( "false" );
            System.out.println("3#) valueAsTernary: " + valueAsTernary );
        }

        System.out.println("4#) valueAsTernary: " + valueAsTernary );
        System.out.println("5#) my static counter: " + logicCounter );

        int k = 28;
        boolean valueMoreThan30 = ( k > 30 ) ?
                Boolean.valueOf( "true" ) :
                Boolean.valueOf( "false" );

        String plusFive = !( k > 30 ) ? new String(""+(k+5)) :
                String.valueOf(k-5);

        System.out.println("valueMoreThan30: " + valueMoreThan30 );
        System.out.println("plusFive: " + plusFive );

        k=28;
        int n = 10;

        System.out.println("Hexvalue variable (before): " + n
                + " HEX: " + Integer.toString( n,16 ).toUpperCase()
                + " \nBIN: " +
                String.format("%016d", new BigInteger(Integer.toString(n, 2))));


        // Interview puzzle
        n >>= ( k > 30 ) ? (k / 2) : Math.abs( ~( k % 3 ) ) ;

        System.out.println("Hexvalue variable: (after) " + n + " push to right " + Math.abs( ~( k % 3 ) )
                + " HEX: " + Integer.toString( n,16 ).toUpperCase()
                + " \nBIN: " +
                String.format("%016d", new BigInteger(Integer.toString(n, 2))));

        System.out.println("shitfright 10: " + k + " n: (DEC) " + n + " (HEX:) " +
                Integer.toString( n,16 ).toUpperCase() );

    } // end main

    static boolean exitAsTrue() {
        System.out.println("Exit as true in static" );
        System.out.println("Static inc: " + logicCounter++);
        return true;
    }; // end

    static boolean exitAsFalse() {
        System.out.println("Exit as false in static" );
        return false;
    }; // end

} // end class