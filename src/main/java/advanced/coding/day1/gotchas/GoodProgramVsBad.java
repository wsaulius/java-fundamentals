package advanced.coding.day1.gotchas;

import java.util.InputMismatchException;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.function.Consumer;

/*
Exercises – data types and variables

  * Sum two integer variables initialized with maximal values for that type.

*/
public class GoodProgramVsBad {

    public static void main(String[] args) {

        // Useless variable : redefined constant in JAVA
        int maxInt = Integer.MAX_VALUE;

        System.out.println("Init MAX_VALUE is: " + maxInt);

        // Overflow:  2 * maxInt
        System.out.println("Integer.MAX_VALUE is: " + (maxInt + maxInt));

        System.out.println("2 x Integer.MAX_VALUE is: " + Integer.MAX_VALUE * 2);
        Scanner reader = new Scanner(System.in);
        System.out.print("Enter char: ");

        // How to read a char in scanner
        char oneChar = reader.next().charAt(0);
        System.out.println("Entered char: " + oneChar);

        System.out.print("Enter byte: ");
        byte oneByte = reader.nextByte();
        System.out.println("Entered byte: " + oneByte);

        reader.nextLine();
        System.out.print("Enter Byte: ");
        Byte twoByte = 0;   // "Boxed" type of byte primitive
        try {
            twoByte = reader.nextByte();
            System.out.println("Entered Byte: " + twoByte);
        } catch (InputMismatchException e) {
            // Good enough programming style
            // "Swallow" exception
            e.printStackTrace();
            System.err.println("Intput error: " + e.getMessage());
            // throw e;
        }

        reader.nextLine();  // Flush the reader.

        System.out.print("Enter float: ");

        float oneFloat = reader.nextFloat();
        System.out.println("Entered float: " + oneFloat);

        reader.nextLine();
        System.out.print("Enter Float: ");

        Float twoFloat = reader.nextFloat();
        System.out.println("Entered Float: " + twoFloat);

        // JAVA8 goodness: available for good programmers only.
        // (the ones that prefer Boxed types instead :)
        final LinkedList<Float> asFloats = new LinkedList<>();
        final Consumer<? super Float> printingConsumer =

                // This method returns a hex string representation of the argument.
                it -> System.out.println(Float.toHexString(it));

        // Implicit converted
        asFloats.add(oneFloat);

        // Added 'as is', "Boxed"
        asFloats.add(twoFloat);

        System.out.println("AS HEX: ");
        asFloats.forEach(printingConsumer);

        Double asSum = asFloats.stream().mapToDouble(floating -> Float.valueOf(floating)).sum();
        System.out.format("AS SUM: %.02f", asSum);

        Long enteredInteger = Math.round(asSum);

        // Examplary if-else if-else
        if (asSum > 30) {
            System.out.println("\nIt's greater then 30");
        } else if (asSum < 30) {
            System.out.println("\nIt's less then 30");
        } else {
            System.out.println("\nIt's 30");
        }

/*      // Over-engineered: not KISS at all
        // Ugly, bad programming style. Still works?
        switch (enteredInteger) {
            case 30:
                System.out.println("It's 30");
                break;

            default: {

                final boolean switchOn = enteredInteger > 30;
                switch ( Boolean.valueOf( switchOn ).toString() ) {

                    case "true":
                        System.out.println("It's greater then 30");
                    break;

                    default:
                        System.out.println("It's less then 30");
                }
                break;
            }
        }*/
    }
}


