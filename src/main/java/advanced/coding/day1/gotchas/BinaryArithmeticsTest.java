package advanced.coding.day1.gotchas;

import java.math.BigInteger;

// Check the results by ///////
/*----- Bitwise Operator Table-

        A   B   A|B A&B A^B ~A
        0   0   0   0   0   1
        1   0   1   0   1   0
        0   1   1   0   1   1
        1   1   1   1   0   0

*/////////////////////////////

public class BinaryArithmeticsTest {

    public static void main(String[] args) {

        // HEXADECIMAL [ 0..15 ]
        // HEX: 00 01 02 ... 09 0A 0B 0C 0D 0E 0F 10 11 12 ... 19 1A 1B 1F ... 20 21 ...

        int hexValue = 0xFF;
        System.out.println("Hexvalue variable: " + hexValue
                + " HEX: " + Integer.toString( hexValue,16 ).toUpperCase()
                + " \nBIN: " +
                String.format("%016d", new BigInteger(Integer.toString(hexValue, 2))));

        System.out.println( "\n---LSHIFT---" );

         // e.g. SHA1, DES, RSA
         for ( int shift = 1; shift < 8; shift++) {
             int shitfLeft = hexValue << shift;
             System.out.println("shitfLeft variable: " + shitfLeft
                     + " HEX: " + Integer.toString( shitfLeft,16 ).toUpperCase()
                     + " \nBIN: " +
                     String.format("%016d", new BigInteger(Integer.toString(shitfLeft, 2))));
         }

        System.out.println( "\n---RSHIFT---" );  // sign

        hexValue = -1 * 0xFF;
        for ( int shift = 1; shift < 8; shift++) {
            int shitfRight = hexValue >> shift;
            System.out.println("shitfLeft variable: " + shitfRight
                    + " HEX: " + Integer.toString( shitfRight,16 ).toUpperCase()
                    + " \nBIN: " +
                    String.format("%016d", new BigInteger(Integer.toString(shitfRight, 2))));
        }

        System.out.println( "\n---RSHIFT UNSIGNED---" );

        hexValue = -0xFF;

        System.out.println("hexValue variable: " + hexValue
                + " HEX: " + Integer.toString( hexValue,16 ).toUpperCase()
                + " \nBIN: " +
                String.format("%032d", new BigInteger(Integer.toString(hexValue, 2))));

        for ( int shift = 1; shift < 32; shift++) {
            int shitfRight = hexValue >>> shift;
            System.out.println("shitfLeft variable: " + shitfRight
                    + " HEX: " + Integer.toString( shitfRight,16 ).toUpperCase()
                    + " \nBIN: " +
                    String.format("%032d", new BigInteger(Integer.toString(shitfRight, 2))));
        }

        System.out.println( "\n---NEGATIVES---" );

        // Negatives
        hexValue = 0xFFE2;

        System.out.println("hexValue variable: " + hexValue
                        + " HEX: " + Integer.toString( hexValue,16 ).toUpperCase()

                        + " \nBIN: " +
                        String.format("%016d", new BigInteger(Integer.toString(hexValue, 2))));

        hexValue = -(0xFFE2);
        System.out.println("hexValue variable: " + hexValue
                    + " HEX: " + Integer.toString( hexValue,16 ).toUpperCase()
                    + " \nBIN: " +
                    String.format("%016d", new BigInteger(Integer.toString(hexValue, 2))));

        System.out.println( "\n---OR---" );

        // Binary OR
        hexValue = 0xFF | 0x01;
        System.out.println("hexValue variable: " + hexValue
                    + " HEX: " + Integer.toString( hexValue,16 ).toUpperCase()

                + " \nBIN: " +
                String.format("%016d", new BigInteger(Integer.toString(0xFF, 2)))

                + " \nBIN: " +
                String.format("%016d", new BigInteger(Integer.toString(0x01, 2)))

                + " \nBIN: " +
                String.format("%016d", new BigInteger(Integer.toString( hexValue, 2))));

        System.out.println( "\n---AND---" );

        // Binary AND
        hexValue = 0xFF & 0x01;
        System.out.println("hexValue variable: " + hexValue
                + " HEX: " + Integer.toString( hexValue,16 ).toUpperCase()

                + " \nBIN: " +
                String.format("%016d", new BigInteger(Integer.toString(0xFF, 2)))

                + " \nBIN: " +
                String.format("%016d", new BigInteger(Integer.toString(0x01, 2)))

                + " \nBIN: " +
                String.format("%016d", new BigInteger(Integer.toString( hexValue, 2))));

        System.out.println( "\n---XOR---" );

        // Binary XOR
        hexValue = 0xFF ^ 0x01;
        System.out.println("hexValue variable: " + hexValue
                + " HEX: " + Integer.toString( hexValue,16 ).toUpperCase()

                + " \nBIN: " +
                String.format("%016d", new BigInteger(Integer.toString(0xFF, 2)))

                + " \nBIN: " +
                String.format("%016d", new BigInteger(Integer.toString(0x01, 2)))

                + " \nBIN: " +
                String.format("%016d", new BigInteger(Integer.toString( hexValue, 2))));

        System.out.println( "\n---NOT---" );

        // Binary NOT
        hexValue = 0xFF ;
        System.out.println("hexValue variable: " + hexValue
                + " HEX: " + Integer.toString( hexValue,16 ).toUpperCase()

                + " \nBIN: " +
                String.format("%016d", new BigInteger(Integer.toString( hexValue, 2)))

                + " \nBIN: " +
                String.format("%016d", new BigInteger(Integer.toString( ~hexValue, 2))));
    }
}
