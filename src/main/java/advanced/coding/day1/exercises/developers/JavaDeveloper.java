package advanced.coding.day1.exercises.developers;

final public class JavaDeveloper extends Developer {

    public JavaDeveloper() {

        // explicit, but not required
        super( );
        System.out.println( "Calling::constructor for " + this + " in class " + " JavaDeveloper " );

    }

    @Override
    // RELAXED encapsulation
    public void code() {
        super.code();

        System.out.println( "I can code JAVA8 too!" );
    }
}
