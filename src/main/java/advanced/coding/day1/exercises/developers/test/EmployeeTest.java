package advanced.coding.day1.exercises.developers.test;
/**
   @version 1.11 2004-02-19
   @author Cay Horstmann
   @author (java8) wsaulius

   // Technical debt: unfinished, imperfect, left alone, sad...
*/

import advanced.coding.day1.exercises.developers.Employee;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.*;
import java.util.function.BiConsumer;
import java.util.stream.IntStream;

// POJO example test
public class EmployeeTest {

   public static void main(String[] args) {

      Employee newEmployee = new Employee();

      newEmployee.setSalary(1400d);
      newEmployee.setHireDay(new Date());
      newEmployee.setName("Bob Brave");

      System.out.println(newEmployee.toString());

      // CEO : Mr. Johnson
      Employee johnson = new Employee("Jack Johnson",
              42000, 1988, 10, 4);

      final LocalDate johnsonHiredDate = johnson.getHireDay().toInstant().atZone(
              ZoneId.systemDefault()).toLocalDate();

      // Encapsulate
      // Private by design : OK
      // johnson.setHireDay(1997, 10, 4);

      johnson.raiseSalary(4);
      System.out.println(johnson);

      // fill the staff array with three Employee objects
      Employee[] staff = new Employee[3];

      staff[0] = new Employee("Carl Cracker", 75000, 1987, 12, 15);
      staff[1] = new Employee("Harry Hacker", 50000, 1989, 10, 1);
      staff[2] = new Employee("Tony Tester", 40000, 1990, 3, 15);

      // raise everyone's salary by 2%
      for (Employee employee : staff) {
         // Accessible within package only
         employee.raiseSalary(2);
      }

      // Generic collections programming
      ArrayList<Employee> staffAsList = new ArrayList<Employee>();
      staffAsList.addAll(Arrays.asList(staff));

      // raise everyone's salary by one more 1%
      for (Employee employee : staffAsList) {
         // Accessible within package only
         employee.raiseSalary(1);
      }

      // JAVA6 way
      System.out.println("\nJAVA6: iterate:\n");
      for (Employee employee : staff) {
         System.out.println( employee );
      }

      // JAVA7 way
      System.out.println("\nJAVA7: iterate:\n");
      Iterator<Employee> ii = staffAsList.iterator();
      while (ii.hasNext()) {

          // Employee "by index"
         System.out.println( ii.next() );
      }

      System.out.println("\nJAVA8: iterate\n");
      // JAVA8 way - just print each: lambda expression
      staffAsList.stream().forEach(e ->
              System.out.println(e)
      );

      System.out.println("\nJAVA8 with range: iterate\n");
      // JAVA8 way - put in indexed collection, no raise
      IntStream.range(0, staff.length)
              .mapToObj(i -> staff[i])
              .collect(HashMap::new,
                      (hash, o) -> hash.put(hash.size(), o), (h, o) -> {
                      })
              .forEach((i, o) -> {
                 System.out.println(i + ") " + o.toString());
              });

      System.out.println("\nJAVA8 with omission: iterate\n");
      // JAVA8 way - print each : Functional programming
      IntStream.range(0, staffAsList.size() )
              .mapToObj(i -> staffAsList.get( i ))
              .forEach( System.out::println );

      System.out.println("\nJAVA8 simplified: iterate\n");
      // JAVA8 way - raise 1% each and print
      IntStream.range(0, staffAsList.size() )
              .mapToObj( staffAsList::get )
              .forEach( (e) -> {
                 e.raiseSalary( 1 );
                 System.out.println( e );
              } );

      // JAVA8 way - raise 2% each and consume
      System.out.println("\nJAVA8 consumer: iterate\n");
      IntStream.range(0, staffAsList.size() )
              .mapToObj( staffAsList::get )
              .forEach( (e) -> {
                 final BiConsumer<Employee, Double> raiseSalary =
                         Employee::raiseSalary;
                 raiseSalary.accept( e, 2d );
                 System.out.println( e );
              } );

      // JAVA8 way - restrict persons that get 2% raise
      System.out.println("\nJAVA8 restrictive: iterate\n");
      IntStream.range(0, staffAsList.size()-2 )
              .mapToObj( staffAsList::get )
              .forEach( (e) -> {
                 e.raiseSalary( 3 );
                 System.out.println( e );
              } );

      // JAVA8 way - raise 5% each and consume: filter only the older employees
      System.out.println("\nJAVA8 consumer: iterate\n");
      IntStream.range(0, staffAsList.size() )
              .mapToObj( staffAsList::get )

              // Only reward the employees that were hired before CEO (Johnson)
              .filter( (older) -> johnson.getHireDay().after( older.getHireDay() ) )
              .forEach( (e) -> {

                 final LocalDate hireDate = e.getHireDay().toInstant().atZone(
                         ZoneId.systemDefault()).toLocalDate();

                 final BiConsumer<Employee, Double> raiseSalary =
                         Employee::raiseSalary;
                 raiseSalary.accept( e, 5d );
                 System.out.println( "VETERAN: " + e.toString() + " working for " +
// TODO: Have a look
// https://www.baeldung.com/java-date-difference
// https://docs.oracle.com/javase/8/docs/api/java/util/Date.html
                         Period.between( hireDate, johnsonHiredDate ).toTotalMonths() +
                                 " months longer than " + johnson.getName()
                 );
              } );
   }
}