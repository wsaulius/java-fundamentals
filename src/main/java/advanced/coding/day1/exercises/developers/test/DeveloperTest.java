package advanced.coding.day1.exercises.developers.test;

import advanced.coding.day1.exercises.developers.Developer;
import advanced.coding.day1.exercises.developers.JavaDeveloper;
import advanced.coding.day1.exercises.developers.Person;

public class DeveloperTest {

    public static void main(String[] args) {

        Person personAsDeveloper = new Developer();
/*

        // DO NOT DO THIS AT HOME !!! GARBAGE COLLECTION
        System.gc();
*/

        new Developer().code();

        Person personAsJavaDev = new JavaDeveloper();
        // Downcasting
        ((Developer)personAsJavaDev).code();

        Developer developerAsJavaDev = new JavaDeveloper();
        developerAsJavaDev.code();

        System.out.println( "init: "  + developerAsJavaDev.getProgramAs() );
        developerAsJavaDev.setProgrammingLanguage( "JAVA9" );

        System.out.println( "after: " + developerAsJavaDev.getProgramAs() );

        JavaDeveloper javaDeveloperAsJavaDev = new JavaDeveloper();
        javaDeveloperAsJavaDev.code();

        System.out.println( "init: "  + javaDeveloperAsJavaDev.getProgramAs() );
        javaDeveloperAsJavaDev.setProgrammingLanguage( "JAVA11" );

        System.out.println( "after: " + javaDeveloperAsJavaDev.getProgramAs() );

    }
}
