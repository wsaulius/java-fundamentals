package advanced.coding.day1.exercises.developers;

public abstract class Person {

    public Person() {
        System.out.println("Calling::constructor for " + this + " in class " + " Person ");
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        System.out.println("Calling::pseudo-destructor for " + this + " in class " + " Person ");
    }
}
