package advanced.coding.day1.exercises.developers;

public class Developer extends Person {

    private String programAs = "C";

    public Developer() {
        // explicit call to default constructor
        super();

        System.out.println("Calling::constructor for " + this + " in class " + " Developer ");
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();

        System.out.println("Calling::pseudo-destructor for " + this + " in class " + " Developer ");
    }

    @Override
    public String toString() {
        return super.toString();
    }

    // RELAXED encapsulation
    public void code() {

        System.out.println("Calling::code for " + this + " in class " + " Developer ");
        System.out.println("I can code in anything!");
    }

    public Person setProgrammingLanguage(final String program) {
        this.programAs = program;
        return this;
    }

    public String getProgramAs() {
        return programAs;
    }

}
