package advanced.coding.day1.exercises.warehouse.test;

import advanced.coding.day1.exercises.warehouse.ShoppingItem;
import advanced.coding.day1.exercises.warehouse.WareHouseLocator;
import advanced.coding.day1.exercises.warehouse.Warehouse;

import java.math.BigDecimal;
import java.util.regex.Pattern;

public class WarehouseTest {

    public static void main(String[] args) {

        Warehouse warehouse = new Warehouse();
        warehouse.getAllOfItems();

        ShoppingItem item = new ShoppingItem( "Hammer",
                new BigDecimal( 6.50f ), 10 );

        warehouse.addShoppingItem( item );

        item = new ShoppingItem( "Nordic Hammer",
                new BigDecimal( 16.75f ), 3 );

        warehouse.addShoppingItem( item );

        warehouse.findShoppingItem( "Hammer" ).
                setPriceOfProduct( new BigDecimal( 9.50f ) )
                .setNameOfProduct( "Newer Hammer" )
                .setCountOfProducts( 17 );

        warehouse.addShoppingItem( new ShoppingItem( "Pliers",
                new BigDecimal( 14.28f ), 5 ) );

        warehouse.updateItem( item,  new ShoppingItem( "Super Hammer",
                new BigDecimal( 16.15f ), 4 ) );

        System.out.println( "Expensive items: " +
                 WareHouseLocator.filterShoppingItems( warehouse.getAllOfItems(),
                        WareHouseLocator.isMoreThan( 10.00 ) ) );

        System.out.println( "Selected items: " +
                 WareHouseLocator.filterShoppingItems( warehouse.getAllOfItems(),
                        // Find items with a space tabulator in the name
                        WareHouseLocator.nameMatches( Pattern.compile( ".*\\s+.*" ) ) ) );

        warehouse.getAllOfItems();

        ShoppingItem oneToDelete = new ShoppingItem( "pliers", new BigDecimal(0f), 0 );

        System.out.println( "Item deleted: " + warehouse.deleteItem( oneToDelete ) );

        warehouse.getAllOfItems();
    }
}
