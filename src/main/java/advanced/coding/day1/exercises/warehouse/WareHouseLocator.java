package advanced.coding.day1.exercises.warehouse;

import advanced.coding.day3.gotchas.CommonsText;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class WareHouseLocator {

    // Initialize logger
    public static Logger log = LogManager.getLogger(WareHouseLocator.class);

    public static Predicate<ShoppingItem> isMoreThan(final Double price) {
        return p -> p.getPriceOfProduct().doubleValue() > price;
    }

    public static Predicate<ShoppingItem> isLessThan(final Double price) {

        System.out.println( "Comparing: " + price );
        return p -> p.getPriceOfProduct().doubleValue() <= price;
    }

    public static Predicate<ShoppingItem> nameMatches(final Pattern pattern) {

        log.debug("PATTERN: " + pattern.toString());
        return p ->
                Pattern.matches(pattern.toString(), p.getNameOfProduct());
    }

    // Find in Generic list using predicates
    public static List<ShoppingItem> filterShoppingItems(
            List<? extends ShoppingItem> allOfItems,
            Predicate<ShoppingItem> predicate) {

        return allOfItems.stream().filter(predicate)
                .collect(Collectors.<ShoppingItem>toList());
    }

}
