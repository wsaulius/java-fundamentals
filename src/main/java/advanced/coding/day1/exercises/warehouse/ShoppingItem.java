package advanced.coding.day1.exercises.warehouse;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;

public class ShoppingItem {

    static DecimalFormat oneFormatForDecimals =
            new DecimalFormat("#,##0.00;(#,##0.00)" );

    // Do not create items! 
    protected ShoppingItem() {
    // System.out.println( "Calling::constructor for " + this + " in class " + " ShoppingItem " );
    }

    public ShoppingItem( final String nameOfProduct, 
                         final BigDecimal priceOfProduct,
                         final Integer countOfProducts ) {
        
        setNameOfProduct( nameOfProduct );
        setPriceOfProduct( priceOfProduct );
        setCountOfProducts( countOfProducts );

        // System.out.println( "Calling::constructor for " + this + " in class " + " ShoppingItem " );
    }

    private String nameOfProduct;
    protected BigDecimal priceOfProduct = BigDecimal.valueOf(0f);
    protected Integer countOfProducts = 0;

    public String getNameOfProduct() {

        System.out.println( "PRODUCT: " + this.nameOfProduct );
        return nameOfProduct;
    }

    public ShoppingItem setNameOfProduct(String nameOfProduct) {
        this.nameOfProduct = nameOfProduct;
        return this;
    }

    public Integer getCountOfProducts() {
        return countOfProducts;
    }

    public ShoppingItem setCountOfProducts(Integer countOfProducts) {
        this.countOfProducts = countOfProducts;
        return this;
    }

    public BigDecimal getPriceOfProduct() {
        return priceOfProduct.setScale(2, RoundingMode.HALF_EVEN);
    }

    public ShoppingItem setPriceOfProduct(BigDecimal priceOfProduct) {
        this.priceOfProduct = priceOfProduct;
        return this;
    }

    // AUTO!
    @Override
    public String toString() {
        return  "ShoppingItem{" +
                "nameOfProduct='" + nameOfProduct + '\'' +
                ", priceOfProduct=" +
                ShoppingItem.oneFormatForDecimals.format( getPriceOfProduct().floatValue() ) +
                ", countOfProducts=" + countOfProducts +
                '}';
    }
}
