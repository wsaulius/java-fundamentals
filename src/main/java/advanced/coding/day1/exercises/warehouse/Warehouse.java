package advanced.coding.day1.exercises.warehouse;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Warehouse {

    private List<ShoppingItem> allOfItems = new ArrayList<>();

    public List<ShoppingItem> getAllOfItems() {

        if (allOfItems.isEmpty()) {
            System.out.println("[]");
        } else {
            // Print as lambda
            // allOfItems.forEach( System.out::println );

            System.out.println(Arrays.asList(allOfItems));
        }
        return allOfItems;
    }

    // For chaining : see PATTERNS
    public Warehouse setAllOfItems(final List<ShoppingItem> allOfItems) {
        this.allOfItems = allOfItems;
        return this;
    }

    // Adding to Generic list
    public Warehouse addShoppingItem(final ShoppingItem item) {

        this.allOfItems.add(item);
        return this;
    }

    // Find in Generic list
    public ShoppingItem findShoppingItem(String nameOfProduct) {

        ShoppingItem returnedItem = new ShoppingItem();

/*
        Iterator ii = allOfItems.iterator();
        while ( ii.hasNext() ) {

            ShoppingItem found  = (ShoppingItem) ii.next();
            System.out.println( "Look into: " + found );

            // Strings to compare
            if ( found.getNameOfProduct().equalsIgnoreCase( nameOfProduct )) {

                System.out.println( "Compare " + found );
                return found;
            }
        }
*/

        for (ShoppingItem found : allOfItems) {

            System.out.println("Look into: " + found);

            // Strings to compare
            if (found.getNameOfProduct().equalsIgnoreCase(nameOfProduct)) {

                System.out.println("Compare " + found);
                return found;
            }
        }

        return returnedItem;
    }

    // Find by name only
    public ShoppingItem findShoppingItem(final ShoppingItem findItem) {
        return this.findShoppingItem(findItem.getNameOfProduct());
    }

    public Boolean updateItem(ShoppingItem findItem, ShoppingItem changeTo) {

        ShoppingItem found = this.findShoppingItem(findItem.getNameOfProduct());
        allOfItems.remove(found);
        return allOfItems.add(changeTo);

    }

    private ShoppingItem raisePrice(ShoppingItem findItem, final Byte percent) {

        ShoppingItem found = this.findShoppingItem(findItem.getNameOfProduct());

        // Plus 10%
        found.setPriceOfProduct(
                found.getPriceOfProduct().add(
                        found.getPriceOfProduct().multiply(BigDecimal.valueOf(0.01 * percent))));

        return found;
    }

    public Boolean deleteItem(ShoppingItem findItem) {

        ShoppingItem found = this.findShoppingItem(findItem.getNameOfProduct());
        return this.allOfItems.remove(found);
    }

}
