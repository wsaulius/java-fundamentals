package advanced.coding.day2.exercises.vehicle;

import advanced.coding.day1.exercises.warehouse.ShoppingItem;

import java.math.BigDecimal;
import java.time.LocalDate;

public abstract class Vehicle extends ShoppingItem {

    protected Vehicle()
    { super(); }

    public Vehicle( final String nameOfProduct, final BigDecimal priceOfProduct, final Integer countOfProducts )
    {
        super( nameOfProduct, priceOfProduct, countOfProducts );
    }

    public abstract Vehicle setVehicleType(VehicleType car);
    public abstract Vehicle setMakeYear(LocalDate year);
    public abstract Vehicle setManufacturer(String maker);

    protected VehicleType vehicleType;

    @Override
    public String toString() {
        return "Vehicle{" +
                "vehicleType=" + vehicleType +
                ", countOfProducts=" + countOfProducts +
                "} " + super.toString();
    }

}
