package advanced.coding.day2.exercises.vehicle;

import advanced.coding.day1.exercises.warehouse.ShoppingItem;

import java.math.BigDecimal;
import java.util.List;

public interface Buyer {

    // Actions: to be invoked
    Vehicle buy(String forName, List<? extends ShoppingItem> inList) throws Exception;
    Vehicle buy(BigDecimal forPrice, List<? extends ShoppingItem> inList) throws Exception;

    BigDecimal getAvailableFunds();
    Buyer setAvailableFunds( BigDecimal funds );

}
