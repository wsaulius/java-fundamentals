package advanced.coding.day2.exercises.vehicle;
import advanced.coding.day1.exercises.developers.Person;

public abstract class BuyerPerson extends Person implements Buyer {

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // Attributes
    private String name;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BuyerPerson{");
        sb.append("name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public BuyerPerson(final String name ) {
        this.setName( name );
    }
}
