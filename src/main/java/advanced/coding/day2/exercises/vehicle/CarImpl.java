package advanced.coding.day2.exercises.vehicle;

import org.apache.commons.lang3.NotImplementedException;

import java.time.LocalDate;

public class CarImpl extends VehicleImpl {

    public CarImpl(VehicleType vehicleType, String vehicleID,
                   String manufacturer,
                   Integer maxSpeed,
                   Integer weight,
                   LocalDate makeYear, double price) {
        super( vehicleType, vehicleID, manufacturer, maxSpeed, weight, makeYear, price );
    }

    public CarImpl() {
            log.info( new NotImplementedException( "Sorry, not quite implemented yet for " + this.getClass()));
        }

    @Override
    public String toString() {
        return "CAR " + super.toString();
    }
}
