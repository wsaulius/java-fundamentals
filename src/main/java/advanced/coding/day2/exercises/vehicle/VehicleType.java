
package advanced.coding.day2.exercises.vehicle;

public enum VehicleType {

    BIKE("Bike"),
    CAR("Car"),
    MOTORCYCLE("Motorcycle"),
    TRACTOR("Tractor"),
    EXCAVATOR("Excavator");

    protected String name;

    // Default constructor for enums is private
    private VehicleType(String name) {
        this.name = name;
    }
}
