package advanced.coding.day2.exercises.vehicle.test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.*;

import advanced.coding.day1.exercises.warehouse.ShoppingItem;
import advanced.coding.day2.exercises.vehicle.*;
import advanced.coding.day2.gotchas.DecimalFormatDemo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static advanced.coding.day2.exercises.vehicle.VehicleType.*;

/*
 * Sukurti Vehicle klasę, kuri privalo turėti:
 * vehycleType(AUTOMOBILE, MOTORCYCLE, TRACTOR, EXCAVATOR),
 * model,
 * weight,
 * maxSpeed,
 * price,
 * makeYear
 *   Atspausdinti transporto priemonės objektą, jei modelio pavadinimas prasideda raide 'A';
 *   Atspausdinti visų AUTOMOBILE TP objektų pagaminimo metus;
 *   Atspausdinti visų MOTORCYCLE TP objektų maksimalų greitį;
 *   Atspausdinti visų TRACTOR TP objektų kainą;
 *   Atspausdinti visų EXCAVATOR TP objektų svorį;
 *   Atspausdinti TP gamintoją, kurių pagaminimo metai vėlesni nei 2018 ir maksimalus greitis mažesnis už 150 myliu per valandą;
 *  */

public class VehicleTest {

    // Initialize logger
    public static Logger log = LogManager.getLogger( DecimalFormatDemo.class );

    private static final double KMH_TO_MPH_MULTIPLIER = 1.6;

    public static void main(String[] args) {

        Vehicle[] vehicles = buildVehicles();

        printVehicleCollection( vehicles );

        //printVehicleWhichManufacturerStartsWith(vehicles, "A");

        printVehicleElementsByType(vehicles, CAR);

        //printVehicleWitchMadeLaterThanAndSlowerThan(
             //   vehicles, 2018, 150);

        final Map<Buyer, BigDecimal> potentialBuyers = new HashMap<>();

        Buyer myPerson = new BuyerPersonImpl("Justas Pikelis",
                BigDecimal.valueOf( 9000L ) );
        System.out.println( myPerson );

        potentialBuyers.put( myPerson, myPerson.getAvailableFunds() );

        System.out.println( potentialBuyers.toString() );

        try {

            final List<ShoppingItem> availableItems = new LinkedList<>( Arrays.asList( vehicles ) );

            Vehicle toBuy =
            // myPerson.buy( myPerson.getAvailableFunds(), availableItems );

            toBuy = myPerson.buy( "BMW:BM[8-9]", availableItems );

            System.out.println( "Buying " + toBuy + " for the price " + toBuy.getPriceOfProduct() );
            availableItems.remove( toBuy );

            // Transact amount
            potentialBuyers.replace( myPerson,
                    myPerson.getAvailableFunds(),
                    myPerson.getAvailableFunds().subtract( toBuy.getPriceOfProduct() ) );

        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.println( potentialBuyers.toString() );

        printVehicleCollection( vehicles );

    }

    private static void printVehicleWhichManufacturerStartsWith(
            Vehicle[] vehicles, String prefix) {

        final Iterator<Vehicle> ii = Arrays.asList(vehicles).iterator();
        while ( ii.hasNext() ) {

            // ii.next normally returns Object
            final VehicleImpl v = (VehicleImpl) ii.next();
            if (v.getManufacturer().startsWith(prefix)) {
                System.out.println(v.toString());
            }
        }
    }

    private static void printVehicleCollection( final Vehicle[] vehicles ) {
/*        Arrays.asList(vehicles).stream().forEach( car ->
                System.out.println( car.toString() ) );*/

        Arrays.asList(vehicles).stream().forEach( System.out::println );
    }

    // Pseudo Builder
    private static void printVehicleElementsByType(
            final Vehicle[] vehicles, final VehicleType type) {

        final Iterator<Vehicle> ii = Arrays.asList(vehicles).iterator();

        while ( ii.hasNext() ) {

            // ii.next normally returns Object
            final VehicleImpl v = (VehicleImpl) ii.next();

            //TODO: Filter out with lambda predicates
            if (type.equals( v.getVehicleType() )) {

                switch (v.getVehicleType()) {

                    case CAR:
                    case BIKE: {
                        System.out.printf("%s: %s %s \n",
                                v.getVehicleType(),
                                v.getMakeYear(),
                                v.getManufacturer() );
                        break;
                    }

                    case MOTORCYCLE: {
                        System.out.printf("%s: %s \n", v.getVehicleType(),
                                v.getMaxSpeed());
                        break;
                    }

                    case TRACTOR: {
                        System.out.printf("%s: %s \n", v.getVehicleType(),
                                v.getPrice());
                        break;
                    }
                    case EXCAVATOR: {
                        System.out.printf("%s: %s \n", v.getVehicleType(),
                                v.getWeight());
                        break;
                    }
                    default:
                        System.err.printf("Not recognized vehicle type %s%n",
                                v.getVehicleType());
                        break;
                }
            }
        }
    }

    private static void printVehicleWitchMadeLaterThanAndSlowerThan(
            final Vehicle[] vehicles_array, int expectedYear, int expectedSpeed) {

        final List<Vehicle> vehicles = Arrays.asList( vehicles_array );

        LocalDate expectedDate = LocalDate.of(expectedYear, 1, 1);

        for (Vehicle vehicle : vehicles) {

            //TODO Downcast : candidate for Adapter pattern
            final VehicleImpl v = (VehicleImpl)vehicle;

            if ((v.getMakeYear().isAfter(expectedDate))
                    && (v.getMaxSpeed() / KMH_TO_MPH_MULTIPLIER < expectedSpeed)) {
                System.out.println(v.getManufacturer());
            }
        }
    }

    private static Vehicle[] buildVehicles() {

        return new Vehicle[]{

                new BikeImpl( BIKE,"Tommy", "Baltic Vairas", 5, 20, LocalDate.of(2013, 8, 12), 300.95),
                new CarImpl( CAR, "A4", "Audi", 187, 1456, LocalDate.of(2016, 5, 12), 2_500),
                new VehicleImpl(TRACTOR, "DZ-203", "Fend", 54, 5621, LocalDate.of(2010, 4, 4), 1_021_456),
                new CarImpl( CAR, "V60", "Volvo", 239, 1680, LocalDate.of(2019, 7, 1), 16_558),
                new VehicleImpl(TRACTOR, "JD-401", "John Deer", 68, 7810, LocalDate.of(2014, 12, 2), 924_880),
                new VehicleImpl(MOTORCYCLE, "BM9", "BMW", 299, 180, LocalDate.of(2008, 5, 21), 7_000),
                new VehicleImpl(EXCAVATOR,  "EX-9", "JCB", 30, 9510, LocalDate.of(2004, 1, 1), 2_000_000),
                new VehicleImpl(MOTORCYCLE, "Laguna", "Ducati", 320, 211, LocalDate.of(2018, 11, 29), 8_500)
        };
    }
}