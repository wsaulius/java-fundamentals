package advanced.coding.day2.exercises.vehicle;

import advanced.coding.day3.gotchas.FascinatingOptionals;
import org.apache.commons.lang3.NotImplementedException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.time.LocalDate;

// POJO : plain old java object
public class VehicleImpl extends Vehicle {

    public static Logger log = LogManager.getLogger( VehicleImpl.class );

    private String manufacturer;

    private Integer maxSpeed;
    private Integer weight;
    private LocalDate makeYear;
    private double price;

    public VehicleImpl(VehicleType vehicleType, String vehicleID,
                       String manufacturer,
                       Integer maxSpeed,
                       Integer weight,
                       LocalDate makeYear, double price) {

        setPriceOfProduct( BigDecimal.valueOf( price ) );
        setNameOfProduct( String.format( "%s:%s", manufacturer, vehicleID ));

        this.vehicleType = vehicleType;
        this.manufacturer = manufacturer;
        this.maxSpeed = maxSpeed;
        this.weight = weight;
        this.makeYear = makeYear;
        this.price = price;
    }

    public VehicleImpl() {
        log.error( new NotImplementedException( "Sorry, not quite implemented yet" ) );
    }

    public VehicleType getVehicleType() {
        return vehicleType;
    }

    @Override
    public Vehicle setVehicleType(VehicleType car) {
        this.vehicleType = car;
        return this;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public Integer getMaxSpeed() {
        return maxSpeed;
    }

    public Integer getWeight() {
        return weight;
    }

    public LocalDate getMakeYear() {
        return makeYear;
    }

    public Double getPrice() {
        return price;
    }

    public Vehicle setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
        return this;
    }

    public Vehicle setMaxSpeed(Integer maxSpeed) {
        this.maxSpeed = maxSpeed;
        return this;
    }

    public Vehicle setWeight(Integer weight) {
        this.weight = weight;
        return this;
    }

    public Vehicle setMakeYear(LocalDate makeYear) {
        this.makeYear = makeYear;
        return this;
    }

    public Vehicle setPrice(double price) {
        super.setPriceOfProduct( BigDecimal.valueOf( price ));
        return this;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "vehicleType=" + vehicleType +
                ", vehicleName=" + getNameOfProduct() +
                ", manufacturer='" + manufacturer + '\'' +
                ", maxSpeed=" + maxSpeed +
                ", weight=" + weight +
                ", makeYear=" + makeYear +
                ", price=" + super.getPriceOfProduct() +
                '}';
    }
}