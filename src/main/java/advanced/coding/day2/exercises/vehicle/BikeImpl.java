package advanced.coding.day2.exercises.vehicle;

import com.ibm.icu.text.RuleBasedNumberFormat;
import com.ibm.icu.util.ULocale;
import org.apache.commons.lang3.StringUtils;

import java.time.LocalDate;

// POJO : plain old java object
public class BikeImpl extends VehicleImpl {

    final protected static ULocale ltLocale = new ULocale("lt_LT");
    private static RuleBasedNumberFormat formatter = new RuleBasedNumberFormat(ltLocale,
            RuleBasedNumberFormat.SPELLOUT);

    public BikeImpl(VehicleType vehicleType, String vehicleID,
                    String manufacturer,
                    Integer maxSpeed,
                    Integer weight,
                    LocalDate makeYear, double price) {
        super(vehicleType, vehicleID, manufacturer, maxSpeed, weight, makeYear, price);
    }

    @Override
    public String toString() {

        return String.format("%s%s%s",
                super.getVehicleType() + ": {",

                "manufacturer='" + super.getManufacturer() + '\'' +
                        ", price=" + StringUtils.capitalize(formatter.format(super.getPrice())),
                "}");
    }
}
