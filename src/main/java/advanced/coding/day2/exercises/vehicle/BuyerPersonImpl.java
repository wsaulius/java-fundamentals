package advanced.coding.day2.exercises.vehicle;

import advanced.coding.day1.exercises.warehouse.ShoppingItem;
import advanced.coding.day1.exercises.warehouse.WareHouseLocator;

import java.math.BigDecimal;
import java.util.List;
import java.util.regex.Pattern;

public class BuyerPersonImpl extends BuyerPerson {

    BigDecimal fundsAvailable = BigDecimal.ZERO;

    public BuyerPersonImpl(String name) {
        super(name);
    }

    public BuyerPersonImpl(String name, BigDecimal fundsAvailable) {
        super(name);
        this.fundsAvailable = fundsAvailable;
    }

    @Override
    public Vehicle buy(BigDecimal forPrice, List<? extends ShoppingItem> shoppingList) throws Exception {

        final List<ShoppingItem> selectedChoice =
        WareHouseLocator.filterShoppingItems( shoppingList,
                WareHouseLocator.isLessThan( forPrice.doubleValue() ) );

        if ( selectedChoice.isEmpty() ) {
            throw new Exception( "No items available for this price: " + forPrice );
        } else {

            System.out.println( "TO SELECT BY PRICE: ");
            selectedChoice.forEach( System.out::println );
        }

        // DO not pass nulls!
        return (Vehicle) selectedChoice.get(0);
    }

    @Override
    public Vehicle buy(String forName, List<? extends ShoppingItem> shoppingList) throws Exception {

        final List<ShoppingItem> selectedChoice =
                WareHouseLocator.filterShoppingItems( shoppingList,
                        WareHouseLocator.nameMatches( Pattern.compile( forName ) ) );

        if ( selectedChoice.isEmpty() ) {
            throw new Exception( "No items available for this name: " + forName );
        } else {

            System.out.println( "TO SELECT BY NAME: ");
            selectedChoice.forEach( System.out::println );
        }

        // DO not pass nulls!
        return (Vehicle) selectedChoice.get(0);
    }

    @Override
    public BigDecimal getAvailableFunds() {
        return this.fundsAvailable;
    }

    @Override
    public Buyer setAvailableFunds(BigDecimal funds) {
        this.fundsAvailable = funds;
        return this;
    }
}
