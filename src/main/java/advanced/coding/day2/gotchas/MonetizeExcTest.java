package advanced.coding.day2.gotchas;

import com.ibm.icu.text.RuleBasedNumberFormat;

import com.ibm.icu.util.ULocale;
import org.apache.commons.lang3.StringUtils;
import org.javamoney.moneta.Money;
import org.javamoney.moneta.format.CurrencyStyle;

import java.nio.charset.Charset;
import java.util.Locale;
import javax.money.*;
import javax.money.format.AmountFormatQueryBuilder;
import javax.money.format.MonetaryAmountFormat;
import javax.money.format.MonetaryFormats;

// International locales and spell-outs

public class MonetizeExcTest {

    final protected static ULocale enLocale = new ULocale("en_EN");
    final protected static ULocale ltLocale = new ULocale("lt_LT");

    final protected static CurrencyUnit euro = Monetary.getCurrency("EUR");

    public static void main(String[] args) {

        MonetaryAmount monetaryAmount = Money.of(new Float(13425.63), euro);
        CurrencyUnit currency = monetaryAmount.getCurrency();

        NumberValue numberValue = monetaryAmount.getNumber();

        MonetaryAmountFormat eurFormat = MonetaryFormats.getAmountFormat(
                AmountFormatQueryBuilder.of(Locale.GERMANY)
                        .set(CurrencyStyle.SYMBOL)
                        .set("pattern", "#,##0.00### ¤")
                        .build());

        RuleBasedNumberFormat formatter = new RuleBasedNumberFormat(enLocale,
                RuleBasedNumberFormat.SPELLOUT);

        String euros =
                StringUtils.capitalize(
                        formatter.format(numberValue.getAmountFractionNumerator()));
        String cent = formatter.format(numberValue.getAmountFractionDenominator());

        String amount = String.format("%s %s %s %s", euros,
                monetaryAmount.getCurrency().getCurrencyCode(), cent, "ct.");

        System.out.println("Amount to be invoiced: " + amount);

        // Reuse for LT locale
        formatter = new RuleBasedNumberFormat(ltLocale, RuleBasedNumberFormat.SPELLOUT);

        euros =
                StringUtils.capitalize(
                        formatter.format(numberValue.getAmountFractionNumerator()));
        final Integer centai = Math.toIntExact(numberValue.getAmountFractionDenominator());

        amount = (String.format("%s %s %d %s", euros,
                monetaryAmount.getCurrency().getCurrencyCode(), centai, "ct."));

        System.out.println( new String( ("S\u0105skaita: "+amount).getBytes(),
                Charset.forName("UTF8")) );
    }
}
