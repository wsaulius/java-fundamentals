package advanced.coding.day2.gotchas;

import java.time.Year;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class LeapYearDemo {

    public static void main(String[] args) {

        try {

            Integer year = 1583;
            do {

                // get the supported ids for GMT-08:00 (Pacific Standard Time)
                String[] ids = TimeZone.getAvailableIDs(-8 * 60 * 60 * 1000);
                // if no ids were returned, something is wrong. get out.
                if (ids.length == 0)
                    System.exit(0);

                // begin output
                System.out.println("Current Time");

/*
                // create a Pacific Standard Time time zone
                SimpleTimeZone pdt = new SimpleTimeZone(-8 * 60 * 60 * 1000, ids[0]);

                // set up rules for Daylight Saving Time
                pdt.setStartRule(Calendar.APRIL, 1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);
                pdt.setEndRule(Calendar.OCTOBER, -1, Calendar.SUNDAY, 2 * 60 * 60 * 1000);

                // create a GregorianCalendar with the Pacific Daylight time zone
                // and the current date and time
                Calendar calendar = new GregorianCalendar(pdt);

*/
                Calendar calendar = new GregorianCalendar();

                System.out.println("Calendar  " + calendar.toString() + " created ");

                if (LeapYearDemo.isLeapYear(year) != Year.of(year).isLeap()) {
                    throw new Exception("Something wrong with this year: " + year);
                }

                // Danger: year++ and ++year
                System.out.printf("Year %d is a leap: %s\n", ++year,

                        // Gotcha!
                        // Yes it is double &&, but works with single & too
                        LeapYearDemo.isLeapYear(year) & Year.of(year).isLeap());

            } while (year <= java.time.Year.now().getValue());

        } catch (Exception e) {

            e.printStackTrace();
            System.err.println(e.getMessage());
        }
    }

    // KISS principle
    public static boolean isLeapYear(int year) {
        assert year >= 1583; // not valid before this date.

        // Good code
        return ((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0);
    }
}