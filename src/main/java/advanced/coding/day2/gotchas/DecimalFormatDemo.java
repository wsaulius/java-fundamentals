package advanced.coding.day2.gotchas;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

public class DecimalFormatDemo {

    // Initialize logger
    public static Logger log = LogManager.getLogger( DecimalFormatDemo.class );

    public static void main(String[] args) {

    log.info( DecimalFormatDemo.class.getSimpleName() + " starts" );

    double x=123456789.226;
    double y=10.3451;

        Locale[] locales = NumberFormat.getAvailableLocales();
        Object lt_locale = new Object();

        for (int i = 0; i < locales.length; ++i) {
            if (locales[i].getCountry().length() == 0) {
                continue; // Skip language-only locales
            }

            log.warn( "LOCALE: " + i + ") " + locales[i].toString() );

            if ( "lt_LT".equalsIgnoreCase( locales[i].toString() )) {

                lt_locale = locales[i];
            }
        }

        try {
            // Null safe, but no nulls: SOLID
            if (StringUtils.isBlank(((Locale) lt_locale).getCountry())) {

                log.info( "LOCALE LT not found!" );
                throw new Exception("Locale for LT could not be found.");
            }

            DecimalFormat df = new DecimalFormat("#,##0.00;(#,##0.00)");

            // Let's do rounding too.
            DecimalFormat pi = new DecimalFormat("#,##0.000000;(#,##0.000000)");

            NumberFormat euro =
                    NumberFormat.getCurrencyInstance( ((Locale) lt_locale) );

            System.out.println(pi.format(Math.PI));

            System.out.println(euro.format(x));

            df = new DecimalFormat(".##");
            System.out.println(df.format(y));

        } catch ( Exception e ) {

            log.error( e.getMessage() );

            e.printStackTrace();
            System.err.println( e.getMessage() );

        }

    }
}
