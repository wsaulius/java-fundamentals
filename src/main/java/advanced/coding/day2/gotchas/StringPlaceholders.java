package advanced.coding.day2.gotchas;

import com.ibm.icu.text.RuleBasedNumberFormat;
import com.ibm.icu.util.ULocale;
import org.apache.commons.lang3.StringUtils;

import java.time.Year;
import java.util.Arrays;
import java.util.Date;

public class StringPlaceholders {

    final protected static ULocale ltLocale = new ULocale("lt_LT");
    private static RuleBasedNumberFormat formatter = new RuleBasedNumberFormat(ltLocale,
            RuleBasedNumberFormat.SPELLOUT);

    static String align( int length ) {

        String alignedTabs = "";
        for ( int i = 0; i <= length; i++ )  {
            alignedTabs += "\t";
        }

        return alignedTabs;

    };

    public static void main(String[] args) {

        String []
        lines = new String[3];

        lines[0] = "Hello world";
        lines[1] = lines[0] + " then ";

        // Placeholder
        lines[2] = String.format( " %f %s %10s",  2f, lines[0], "now" );

        for ( int i = 0; i <= 2; i++ ) {
                System.out.println( "["+i+"]: " + align( 5 ) + lines[i]);
        }

        System.out.printf("The date is %1$tm ( %<tB ) / %<te ( %<tA ) / %<tY%n",
                new Date( ) );

        final Integer yearNow = Year.now().getValue() + 10;
        String numberLT = StringUtils.capitalize( formatter.format( yearNow ) );

        // TODO: Proper LT ordinals // TODO
        System.out.println(
        String.format(  // First and naive :)
                ( Arrays.asList( new Character [] { 't' } ).contains(
                        numberLT.charAt( numberLT.length()-1 ) ) ) ?
                        "%sieji metai" : "%s", numberLT ));

        System.out.println( Arrays.asList( lines ));
    }


}
