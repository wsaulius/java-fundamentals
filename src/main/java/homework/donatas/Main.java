package homework.donatas;

import homework.donatas.Exercises_No_7_8.Map_of_travel;
import homework.donatas.Exercises_No_7_8.ValidValueException;

import java.util.Map;

public class Main {
    public static void main(String[] args) {

        Map_of_travel vacation = new Map_of_travel();

        final Map<String, String> vacationMap = vacation.setMapOfTravel();
        vacation.printMapOfTravel();

        try {
            vacation.getNext();

        } catch (ValidValueException exception) {
            System.err.println(exception.getMessage());
        }

        vacationMap.entrySet().forEach(System.out::println);
    }
}
