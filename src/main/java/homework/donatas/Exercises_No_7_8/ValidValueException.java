package homework.donatas.Exercises_No_7_8;

public class ValidValueException extends Exception {

    public ValidValueException(String message) {
        super(message);
    }
}
