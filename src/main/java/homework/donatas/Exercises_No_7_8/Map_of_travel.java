package homework.donatas.Exercises_No_7_8;

import org.json.simple.JSONObject;
import java.util.*;

public class Map_of_travel implements Vacation, Country {

    private String country;
    private String city;

    private final Map<String, String> mapOfTravel = new HashMap<String, String>();
    private final List<JSONObject> listOfJSON = new ArrayList<>();

    private JSONObject jsonMap = new JSONObject();

    @Override
    public Country setCountry(String country) {
        this.country = country;
        return this;
    }

    @Override
    public String getCountry() {
        return country;
    }

    @Override
    public Country setCity(String city) {
        this.city = city;
        return this;
    }

    @Override
    public String getCity() {
        return city;
    }

    // Enter pairs <Country:City>
    public Map<String, String> setMapOfTravel() {
        while ( true ) {

            System.out.println("Enter a Country: ");

            Scanner scanner = new Scanner(System.in);

            String stringOfCountry = scanner.nextLine();

            System.out.println("Enter a City: ");

            Scanner scanner2 = new Scanner(System.in);

            String stringOfCity = scanner2.nextLine();

            // Bitwise &
            if (stringOfCountry.isEmpty() & stringOfCity.isEmpty()) {
                // Interrupt the loop
                break;
            }

            if (mapOfTravel.containsValue(stringOfCity) ||
                    mapOfTravel.containsKey(stringOfCountry)) {

                System.out.println("back and forth");
            } else {
                setCountry(stringOfCountry);
                setCity(stringOfCity);
                mapOfTravel.put(getCountry(), getCity());
            }
        }
        return mapOfTravel;
    }

    public void printMapOfTravel() {

        // Linear, iterable collection
        mapOfTravel.entrySet().stream();

        mapOfTravel.forEach((key, value) ->
                System.out.println("Country: " + key + " --> City: " + value));
    }

    private Object setToJSONMAP( final JSONObject map ) {
        jsonMap.putAll( map );
        return jsonMap;
    }

/*    public JSONObject getJSONMAP() {
        // setToJSONMAP();
        return jsonMap;
    }*/

    public void getNext() throws ValidValueException {

        for (Map.Entry<String, String> entry : mapOfTravel.entrySet()) {
            System.out.println("Command: \u001B[34mNext \u001B[0m(next vacation) \u001B[34m\nexit \u001B[0m(Out of the vacation list ");
            Scanner scanner = new Scanner(System.in);

            String command = scanner.nextLine();

            if (command.equalsIgnoreCase("Next")) {
                System.out.println(entry);
            } else if (command.equalsIgnoreCase("Exit")) {
                break;
            } else {
                throw new ValidValueException("\u001B[31mValid value !!");
            }
        }
        removeVacation();
        mapOfTravel.forEach((key, value) ->
                System.out.println("Country: " + key + " --> City: " + value));
    }

    private Map<String, String> removeVacation() throws ValidValueException {

        for (Map.Entry<String, String> entry : mapOfTravel.entrySet()) {
            System.out.println("Command: \u001B[34mDelete \u001B[0m(Remove vacation, user was not able to visit it.)" +
                    "\n\u001B[34m\nexit \u001B[0m(Out of the vacation list");
            Scanner scanner = new Scanner(System.in);
            String command = scanner.nextLine();
            if (command.equalsIgnoreCase("Delete")) {
                mapOfTravel.remove(entry);
            } else if (command.equalsIgnoreCase("Exit")) {
                break;
            } else {
                throw new ValidValueException("\u001B[31mValid value !!");
            }

        }
        return mapOfTravel;
    }
}
