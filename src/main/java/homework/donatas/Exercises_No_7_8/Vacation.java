package homework.donatas.Exercises_No_7_8;

public interface Vacation {

    Country setCountry(String country);
    String getCountry();
}
