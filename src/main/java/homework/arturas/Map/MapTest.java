package homework.arturas.Map;

import java.util.*;

public class MapTest {

    public static void main(String[] args) {

        Map<String,String> employeeManagerList = new HashMap<>();
        employeeManagerList.put("Employee","Manager");

        for (Map.Entry e:employeeManagerList.entrySet()){
            System.out.println(e.getKey());
        }
        System.out.println("============================");

        Map<Employee,Manager> employeeManagerList1 = new HashMap<Employee, Manager>();
        Employee employee = new Employee("Murininkas");
        Manager manager = new Manager("Brigadininkas");

        employeeManagerList1.put(employee,manager);

/*        for(Map.Entry<Employee,Manager> item:employeeManagerList1.entrySet()){
            System.out.println(item.getValue().getName());
        }
*/
        // Alternatively
        employeeManagerList1.entrySet().forEach( System.out::println );

        System.out.println("============================");

        Map<Manager, List<Employee>> employeeManagerList2 = new Hashtable<Manager, List<Employee>>();

        List<Employee> employeeList = new LinkedList<Employee>();
        employeeList.add(new Employee("Murininkas"));
        employeeList.add(new Employee("Dailide"));
        employeeList.add(manager);
        employeeList.add( new Employee( "Dazytojas") );

        employeeManagerList2.put(manager,employeeList);
        for (Map.Entry<Manager,List<Employee>> item : employeeManagerList2.entrySet()){
            item.getValue().stream().sorted().forEach( System.out::println );
        }
        System.out.println("===========================");

    }

}
