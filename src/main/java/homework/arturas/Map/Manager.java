package homework.arturas.Map;

public class Manager extends Employee {

    private static final String idManager = "4005";

    public Manager(String name){
        super( name );
    }

    // Explicit
    @Override
    public String toString() {
        return String.format( "ID class %s: %s", idManager,
                super.toString() );
    }

}
