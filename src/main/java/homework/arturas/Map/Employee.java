package homework.arturas.Map;

public class Employee implements Comparable {

    // Re-use: show in sub-class
    protected String name;

    public Employee(final String name){
        this.name = name;
    }

    @Override
    public String toString() {
        final StringBuffer sb = new StringBuffer( this.getClass().getSimpleName() );
        sb.append("{");
        sb.append("name='").append(name).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public String getName() {
        return name;
    }

    public Employee setName(String name) {
        this.name = name;
        return this;
    }

    @Override
    public int compareTo(Object o) {
        return this.getName().compareTo( ((Employee)o).getName() );
    }
}
