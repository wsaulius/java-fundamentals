package advanced.coding.day1.exercises.warehouse.test;

//  https://junit.org/junit5/docs/current/user-guide/

import advanced.coding.day1.exercises.warehouse.ShoppingItem;
import advanced.coding.day1.exercises.warehouse.Warehouse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.*;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

@Tag("WareHouseTest")
public class JUnitCallWareHouseTest extends BasicMyTests {

    public static Logger log = LogManager.getLogger(JUnitCallWareHouseTest.class);

    // Until we learn mocking
    private static Warehouse warehouse = null;

    @BeforeAll
    static void initAll() {
        log.info( "BeforeAll::initAll" );
        warehouse = new Warehouse();
    }

    @AfterAll
    static void tearDownAll() {
        log.info( "AfterAll::tearDownAll" );
    }

    @BeforeEach
    void init() {
        log.info( "BeforeEach::init" );
    }

    @Test
    void succeedingTest() {
        log.info( "succeedingTest::OK" );
    }

    @Test
    @Disabled("make all tests pass")
    void failingTest() {
        fail("a failing test");
        log.info( "Disabled::failingTest" );
    }

    @Test
    @Disabled("for demonstration purposes")
    void skippedTest() {
        // not executed
        log.info( "Disabled::skippedTest" );
    }

    @Test
    void abortedTest() {
        assumeTrue( "abc".contains("Z") );
        log.error( "Test::abortedTest" );
        fail("test should have been aborted");
    }

    @AfterEach
    void tearDown() {
        log.info( "AfterEach::tearDown" );
    }

    @DisplayName("Custom test: create Warehouse")
    @Test
    @Order(1)
    void createWarehouseTest() {

        assertNotNull( warehouse, "Warehouse object created." );
        warehouse.getAllOfItems();
    }

    @DisplayName("Custom test: add item to Warehouse")
    @Test
    @Order(2)
    void createItemsTest() {

        assertNotNull( warehouse, "Warehouse initialized." );
        warehouse.getAllOfItems();

        ShoppingItem item = new ShoppingItem( "Testing Hammer",
                new BigDecimal( 6.50f ), 10 );

        warehouse.addShoppingItem( item );

        item = new ShoppingItem( "Hammer",
                new BigDecimal( 16.75f ), 3 );

        warehouse.addShoppingItem( item );

        ShoppingItem foundItem =
        warehouse.findShoppingItem( "Hammer" ).
                setPriceOfProduct( new BigDecimal( "9.55" ) )
                .setNameOfProduct( "Hammer Discount" )
                .setCountOfProducts( 4 );

        warehouse.getAllOfItems();
        assertEquals( new BigDecimal( 9.55f )
                .setScale(2, RoundingMode.HALF_DOWN ),
        foundItem.getPriceOfProduct() );
    }

}
