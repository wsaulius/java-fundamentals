package advanced.coding.day1.exercises.warehouse.test;

import advanced.coding.day1.exercises.warehouse.Warehouse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.junit.jupiter.api.*;

class BasicMyTests {

    public static Logger log = LogManager.getLogger(BasicMyTests.class);

    @BeforeAll
    static void initAll() {
        log.info( "BasicMyTests :: BeforeAll::initAll" );
    }

}
