package ConstructorTest; /**
   @version 1.01 2004-02-19
   @author Cay Horstmann
   @author wsaulius (finalized Employee) + tracing
*/

import java.util.*;

 public class ConstructorTest
{
   public static void main(String[] args)
   {
      // fill the staff array with three Employee objects
      Employee[] staff = new Employee[3];

      staff[0] = new Employee("Harry", 40000);
      staff[1] = new Employee(60000);

      staff[2] = new Employee( "Sally" );

      // Default constructor: not accessible
      // staff[3] = new Employee();

      // print out information about all Employee objects
      for (Employee e : staff)
         System.out.println("name=" + e.getName()
            + ",id=" + e.getId()
            + ",salary=" + e.getSalary());
   }
}

final class Employee
{
   // three overloaded constructors: designated
   public Employee(String n, double money)
   {
      name = n;
      salary = money;
      System.out.println( "Creating Employee overloaded: " + this.getClass() + " for " + n );
   }

   protected Employee(double money)
   {
      // calls the Employee() constructor
      this.salary = money;
      this.name = "Employee #" + id;
      System.out.println( "Creating : " + this.name );
   }

   protected Employee(String name) {
      // calls the Employee() constructor
      this();
      this.name = name;
      System.out.println( "Creating Employee name: " + this.name );

   }

   // the Default constructor, yes it is private!
   private Employee()
   {
      // salary explicitly is set--initialized to 0
      // id initialized in initialization block

      this.salary = 670d;
      System.out.println( "Creating Employee default: " + this.getClass() );
   }

   public String getName()
   {
      return name;
   }

   public double getSalary()
   {
      return salary;
   }

   public int getId()
   {
      return id;
   }

   private static int nextId;

   private int id;
   private String name = ""; // instance field initialization
   private double salary;

   // static initialization block
   static
   {
      System.out.println( "Called static init block {begin}: " + nextId );

      Random generator = new Random();
      // set nextId to a random number between 0 and 9999
      Employee.nextId = generator.nextInt(10000);

      System.out.println( "Called static init block {end}: " + nextId );
   }

   // object initialization block
   // general for all the created objects.
   {
      System.out.println( "Called after each object constructor {begin}: " + nextId );

      this.id = Employee.nextId;
      Employee.nextId++;

      System.out.println( "Called after each object constructor {end}: " + nextId );
   }
}
