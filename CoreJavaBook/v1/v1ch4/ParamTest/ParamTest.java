package ParamTest;

/**
   @version 1.00 2000-01-27
   @author Cay Horstmann
*/

public class ParamTest
{
   public static void main(String[] args)
   {
      /*
         Test 1: Methods can't modify numeric parameters
      */
      System.out.println("Testing tripleValue:");
      double percent = 10;
      System.out.println("Before: percent=" + percent);
      tripleValue(percent);
      System.out.println("After: percent=" + percent);

      /*
         Test 2: Methods can change the state of object
         parameters
      */
      System.out.println("\nTesting tripleSalary:");

      Employee harry = new Employee("Harry", 50000);
      System.out.println("Before: salary=" + harry.getSalary());
      tripleSalary(harry);
      System.out.println("After: salary=" + harry.getSalary());

      /*
         Test 3: Methods can't attach new objects to
         object parameters
      */
      System.out.println("\nTesting swap:");
      Employee a = new Employee("Alice", 70000);
      Employee b = new Employee("Bob", 60000);

      System.out.println("Before: a=" + a.getName());
      System.out.println("Before: b=" + b.getName());

      // Change the values: A->B, B->A
      swap( a, b );

      // Generic! : implicit upcast
      swap( (Object)(Employee) a, (Object) b );

      // Generic!
      swap( (Object) a, (Employee) b );

      System.out.println("After: a=" + a.getName());
      System.out.println("After: b=" + b.getName());

      String strA =
      new String( "String value A");

      String strB =
      new String( "String value B");

      System.out.println("Before: strA=" + strA );
      System.out.println("Before: strB=" + strB );

      // Polymorphic swap
      swap( strA, strB );

      swap( new Integer( 10 ), new Integer( 20 ) );

      swap( new Float( 11.01f ), new Float( 12.02f ) );

   }

   protected static void tripleValue(double x) // doesn't work
   {
      x = 3 * x;
      System.out.println("End of method: x=" + x);
   }

   // Pass Employee as a type. that is: new Class becomes a type
   public static void tripleSalary(final Employee x) // works
   {
      x.raiseSalary(200);
      System.out.println("End of method: salary="
         + x.getSalary());
   }

   // Polymorphic overloading
   protected static void swap( Employee x, Employee y ) {

      System.out.println( "We are swapping Employee objs now!" );

      // Check the type first, do we still need it ??
      if (x instanceof Employee && y instanceof Employee) {

         Employee temp = x;

         x = y;
         y = temp;

         System.out.println("End of method for " + x.getClass() + " : x=" + x.getName());
         System.out.println("End of method for " + y.getClass() + " : y=" + y.toString());
      }
   }

   public static void swap( Object x, Object y )
   {
         System.out.println( "Lets swap objects now" );

         Object temp = x;
         x = y;
         y = temp;

         System.out.println("End of method: x Object =" + x.toString() );
         System.out.println("End of method: y Object =" + y.toString());
      }
}
