package ParamTest;

/*
 Object
   |->
     Employee

*/
class Employee // simplified Employee class
{
    public Employee(String n, double s)
    {
        name = n;
        salary = s;
    }

    public String getName()
    {
        return name;
    }

    public double getSalary()
    {
        return salary;
    }

    public void raiseSalary(double byPercent)
    {
        double raise = salary * byPercent / 100;
        salary += raise;
    }

    private String name;
    private double salary;

    // Let's define a different way to print this as Object
    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder( "name=");

        stringBuilder
                .append( this.getName() )
                .append( ",salary=" + this.getSalary() );

        return stringBuilder.toString();
    }
}
