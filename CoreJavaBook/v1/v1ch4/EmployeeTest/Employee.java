package EmployeeTest;

import java.util.Date;
import java.util.GregorianCalendar;

// POJO : Plain old java object and encapsulation
class Employee
{
    // Attributes
    private String name;
    private Double salary;
    private Date hireDay;

// By default this constructor will be generated
    public Employee( ) {

        this.name = new String();
        this.salary = new Double(0l);
        this.hireDay = new Date( 0l );   // 1970-01-01
    }

    // Constructor
    public Employee(String name, double salary, int year, int month, int day)
    {
        // Call setters inside
        setName( name );
        setSalary( salary );
        setHireDay(year, month, day);
    }

    public String getName()
    {
        return name;
    }

    public double getSalary()
    {
        return salary;
    }

    public Date getHireDay()
    {
        return hireDay;
    }

    public void setHireDay(Date hireDay) {
        this.hireDay = hireDay;
    }

    // Override setter with private method
    private void setHireDay(int year, int month, int day) {

        final GregorianCalendar calendar = new GregorianCalendar(year, month - 1, day);
        // GregorianCalendar uses 0 for January
        setHireDay( calendar.getTime() );
    }

    // Methods are allowed to be accessed (from same package)
    protected void raiseSalary(double byPercent)
    {
        double raise = salary * byPercent / 100;
        this.salary += raise;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {

        StringBuilder stringBuilder = new StringBuilder( "name=");

        stringBuilder
                .append( this.getName() )
                .append( ",salary=" + this.getSalary() )
                .append( ",hireDay=" + this.getHireDay()
                );
        return stringBuilder.toString();
    }
}
