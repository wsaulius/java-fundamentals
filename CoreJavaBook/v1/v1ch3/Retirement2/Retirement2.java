package Retirement2; /**
   @version 1.20 2004-02-10
   @author Cay Horstmann
*/

import java.util.*;
public class Retirement2
{
   public static void main(String[] args) {
      Scanner in = new Scanner(System.in);

      System.out.print("How much money will you contribute every year? ");

      double payment = 0;

      try {    // try -catch block
       payment = in.nextDouble();
       in.nextLine();  // read the newline
      }
         catch ( InputMismatchException e ) {
            System.err.printf("You input is not valid %s\n", e );
      }

      System.out.print("Interest rate in %: ");

      double interestRate = 0;
      try {    // try -catch block
         interestRate = in.nextDouble();
         in.nextLine();  // read the newline

         double balance = 0;
         int year = 0;

         String input;

         final String validForQuit =
                 "[Y|y|q|Q|e|E|\\\\BExit\\\\B|q|\\\\BQuit\\\\B|\\\\Bquit\\\\B|\\\\BFinish\\\\B|\\\\Bfinish\\\\B]{1,6}";

         final String validForContinue =
                 "[Y|y|N|n|\\\\BYes\\\\B|\\\\Byes\\\\B|\\\\BNo\\\\B|\\\\Bno\\\\B|\\\\BYES|\\\\BNO]";
         boolean flag = false;

         // update account balance while user isn't ready to retire
         do
         {
            // add this year's payment and interest
            balance += payment;
            double interest = balance * interestRate / 100;
            balance += interest;

            year++;

            // print current balance
            System.out.printf("After year %d, your balance is %,.2f%n", year, balance);

            // ask if ready to retire and get input
            System.out.print("Ready to retire? (Y/N) ");
            input = in.next();

            flag = input.matches( validForContinue );

            if ( !flag ) {
               System.out.printf("You must enter %s\n", validForContinue );
            }

         }
         while ( !input.matches( validForQuit ) );
      }
      catch ( InputMismatchException e ) {
         System.err.printf("You input is not valid %s\n", e );
         System.err.printf("Exit.\n" );

      }



   }
}