package EnumTest; /**
   @version 1.0 2004-05-24
   @author Cay Horstmann
*/

import java.util.*;


// Advanced features course
public class EnumTest
{  
   public static void main(String[] args)
   {  
      Scanner in = new Scanner(System.in);
      System.out.print("Enter a size: (SMALL, MEDIUM, LARGE, EXTRA_LARGE, DEFAULT_SIZE) ");

      // Like: MEDIUM
      String input = in.next().toUpperCase();

      Size size = Enum.valueOf( Size.class, input );

      System.out.println("size=" + size);
      System.out.println("abbreviation=" + size.getAbbreviation());

      if (size == Size.EXTRA_LARGE) {
         System.out.println("Good job--you paid attention to the _.");
      }

      if ( size == Size.DEFAULT_SIZE )  {
         System.out.println("You get Default size which is " +
                 size.getAbbreviation() );
      }
   }
}

// T-shirt sizes: enumerators (enumeration)
//
// Size.SMALL == small size T-Shirt
//
enum Size
{
   SMALL("S"),
   MEDIUM("M"),
   LARGE("L"),
   EXTRA_LARGE("XL"),
   DEFAULT_SIZE;

   // Default constructor
   Size() {
      this.abbreviation = "M";
   }

   // Constructor !
   private Size(String abbreviation) {
      this.abbreviation = abbreviation;
   }

   // Getter
   public String getAbbreviation() {
      return abbreviation;
   }

   // private member field: for enum
   private String abbreviation;
}
