package ArrayGrowTest; /**
   @version 1.01 2004-02-21
   @author Cay Horstmann
*/

import java.lang.reflect.*;

public class ArrayGrowTest
{  
   public static void main(String[] args)
   {
      // Let's suppose we want to have an array of 5
      int[] array = { 1, 2, 3 };

      // Overload as goodArrayGrow
      array = (int[]) goodArrayGrow( array );
      arrayPrint(array);

      String[] strings = { "Tom", "Dick", "Harry" };
      strings = (String[]) goodArrayGrow( strings );
      arrayPrint(strings);

      Employee[] staff = {
              new Employee("Sally", 2000d, 1980,11,15 ),
              new Employee("Harry", 1000d, 1980,1,3 ),
              new Employee("Anthony", 3000d, 1980,9,24 ) };

      staff = (Employee[]) goodArrayGrow( staff );
      arrayPrint(staff);

     System.err.println("The following call will generate an exception.");

     try {
        strings = (String[]) badArrayGrow(strings);
     } catch ( ClassCastException e ) {
        throw new AssertionError( "Classes are not compatible:", e );
     }
   }

   /**
      This method attempts to grow an array by allocating a
      new array and copying all elements. 
      @param a the array to grow
      @return a larger array that contains all elements of a.
      However, the returned array has type Object[], not
      the same type as a
   */
   static Object[] badArrayGrow(Object[] a)
   {  
      int newLength = a.length * 11 / 10 + 10;
      Object[] newArray = new Object[newLength];
      System.arraycopy(a, 0, newArray, 0, a.length);
      return newArray;
   }

   /**
      This method grows an array by allocating a
      new array of the same type and copying all elements. 
      @param generic the array to grow. This can be an object array
      or a fundamental type array
      @return a larger array that contains all elements of a.

   */

/*

   // NOT TO REWRITE AS:
   static int[] goodArrayGrow(int[] a) {
      return new int[10];
   }

   static String[] goodArrayGrow(String s[]) {
      return new String[10];
   }

*/

   // Generic objects operated
   static Object goodArrayGrow(Object generic)
   {

      // REF: Oracle java8 java.lang.Class
      Class cl = generic.getClass();

      // Check if generic passed has a class of Array type
      if (!cl.isArray()) {
         return null;
      }

      // Go on...
      // Can be: Float, Integer, int, byte, String, Employee
      Class componentType = cl.getComponentType();

      // How many of these are there? let's say 3.
      int length = Array.getLength(generic);

      // OK, multiply, add 10 ==> bigger at least 2-3 times
      int newLength = length * 11 / 10 + 10;

      // Construct a new array : of any type (generics!)
      // new String[ 13 ]
      // OR:
      // new Employee[ 23 ]
      // OR:
      // new boolean[ 124 ]

      Object newArray = Array.newInstance(componentType, newLength);

      // Remember ((buckets!)) fill the space
      System.arraycopy(generic, 0, newArray, 0, length);

      // Brand new array
      return newArray;
   }

   /**
      A convenience method to print all elements in an array
      @param a the array to print. can be an object array 
      or a fundamental type array
   */
   static void arrayPrint(Object a)
   {  
      Class cl = a.getClass();
      if (!cl.isArray()) return;
      Class componentType = cl.getComponentType();
      int length = Array.getLength(a);
      System.out.print(componentType.getName()
         + "[" + length + "] = { ");
      for (int i = 0; i < Array.getLength(a); i++)
         System.out.print(Array.get(a, i) + " ");
      System.out.println("}");
   }
}
