package ObjectAnalyzerTest; /**
   @version 1.11 2004-02-21
   @author Cay Horstmann
*/

import java.lang.reflect.*;
import java.util.*;
import java.text.*;

public class ObjectAnalyzerTest
{  
   public static void main(String[] args)
   { 
      ArrayList<Integer> squares = new ArrayList<Integer>();
      for (int i = 1; i <= 5; i++) squares.add(i * i);
      System.out.println(new ObjectAnalyzer().toString(squares));
   }
}

