package SystemInfo;

import java.util.*;

/**
   This program prints out all system properties.
*/
public class SystemInfo
{  
   public static void main(String args[])
   {   
      Properties systemProperties = System.getProperties();
      Enumeration enumerated = systemProperties.propertyNames();
      while (enumerated.hasMoreElements())
      {
         String key = (String)enumerated.nextElement();
         System.out.println(key + "=" +
            systemProperties.getProperty(key));
      }
   }
}
