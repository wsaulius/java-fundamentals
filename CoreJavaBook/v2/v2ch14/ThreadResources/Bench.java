package ThreadResources;

public class Bench {

    private int availableSeats;

    public Bench(int availableSeats) {
        this.availableSeats = availableSeats;
    }

    public void takeASeatPolitely() {
        if (availableSeats > 0) {
            System.out.println("Taking a seat.");
            availableSeats--;
            System.out.println("Free seats left " + availableSeats);
        } else {
            System.out.println("There are no available seats. :(");
        }
    }

    public synchronized void takeASeat() {
        if(availableSeats > 0) {
            System.out.println("Taking a seat.");
            availableSeats--;
            System.out.println("Free seats left " + availableSeats);

            methodWithSyncedCodeBlock();
        } else {
            System.out.println("Cannot take a seat. :(");
        }
    }

    public void methodWithSyncedCodeBlock() {

        System.out.println("Unsynced part");
        synchronized (this) {
            System.out.println("Synced part");

        }
    }


}