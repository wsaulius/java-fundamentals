package GenericsTest;
// A Simple Java program to show multiple
// type parameters in Java Generics

// We use < > to specify Parameter type
class BinaryGeneric<T, U>
{
    T obj1; // An object of type T
    U obj2; // An object of type U

    // constructor
    BinaryGeneric(T obj1, U obj2)
    {
        this.obj1 = obj1;
        this.obj2 = obj2;
    }

    // To print objects of T and U
    public void print()
    {
        System.out.println(obj1);
        System.out.println(obj2);
    }
}

// Driver class to test above
class BinaryGenerics
{
    public static void main (String[] args)
    {
        BinaryGeneric <String, Integer> obj =
                new BinaryGeneric<String, Integer>("AsString", new Integer( 15 ) );

        obj.print();
    }
}
