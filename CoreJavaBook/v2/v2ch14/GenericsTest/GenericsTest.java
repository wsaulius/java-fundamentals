package GenericsTest;
// A Simple Java program to show working of user defined
// Generic classes

// Driver class to test above
class GenericsTest
{
    public static void main (String[] args)
    {
        // instance of Integer type
        GenericNested <Integer> iObj = new GenericNested<Integer>(15);
        System.out.println(iObj.getObject());

        // instance of String type
        GenericNested <String> sObj =
                new GenericNested<String>("NestedAsString");
        System.out.println(sObj.getObject());
    }

    // We use < > to specify Parameter type
    static class GenericNested<T>
    {
        // An object of type T is declared
        T obj;
        GenericNested(T obj) { this.obj = obj; } // constructor
        public T getObject() { return this.obj; }
    }



}
