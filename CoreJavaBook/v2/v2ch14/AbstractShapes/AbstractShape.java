package AbstractShapes;

abstract class AbstractShape {

    private String name;

    public AbstractShape() {
        this.name = "Unknown shape";
    }

    public AbstractShape(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // Abstract methods
    public abstract void draw();

    public abstract double getArea();

    public abstract double getPerimeter();
}
