package BoundGenerics;

class MultipleBoundGeneric<T extends ClassA & InterfaceB>
{

    private T objRef;

    public MultipleBoundGeneric(T obj){
        this.objRef = obj;
    }

    public void doRunTest(){
        this.objRef.displayClass();
    }
}

interface InterfaceB
{
    public void displayClass();
}

class ClassA implements InterfaceB
{
    public void displayClass()
    {
        System.out.println("Inside super class ClassA");
    }
}

public class MultipleBoundGenerics
{
    public static void main(String a[])
    {
        //Creating object of sub class ClassA and
        //passing it to MultipleBoundGeneric as a type parameter.
        MultipleBoundGeneric<ClassA> bea = new MultipleBoundGeneric<ClassA>(new ClassA());
        bea.doRunTest();

    }
}
