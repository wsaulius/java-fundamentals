package BoundGenerics;// This class only accepts type parameters as any class
// which extends class A or class A itself.
// Passing any other type will cause compiler time error

class BoundGeneric<T extends A>
{

    private T objRef;

    public BoundGeneric(T obj){
        this.objRef = obj;
    }

    public void doRunTest(){
        this.objRef.displayClass();
    }
}

class A
{
    public void displayClass()
    {
        System.out.println("Inside super class A");
    }
}

class B extends A
{
    public void displayClass()
    {
        System.out.println("Inside sub class B");
    }
}

class C extends A
{
    public void displayClass()
    {
        System.out.println("Inside sub class C");
    }
}

public class BoundGenerics
{
    public static void main(String a[])
    {

        // Creating object of sub class C and
        // passing it to BoundGeneric as a type parameter.
        BoundGeneric<C> bec = new BoundGeneric<C>(new C());
        bec.doRunTest();

        // Creating object of sub class B and
        // passing it to BoundGeneric as a type parameter.
        BoundGeneric<B> beb = new BoundGeneric<B>(new B());
        beb.doRunTest();

        // similarly passing super class A
        BoundGeneric<A> bea = new BoundGeneric<A>(new A());
        bea.doRunTest();

    }
}
