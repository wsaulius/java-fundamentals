package LambdaTest;

import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.Consumer;

import java.util.function.UnaryOperator;

class LambdaTest {

    public static void main(String[] args) {

        Runnable myRunnable = () -> System.out.println("Running a runnable");
        myRunnable.run();

        Predicate<String> startsWithABCTest = s -> s.startsWith("ABC");
        System.out.println(startsWithABCTest.test("ABCDEF"));

        // This function returns a random value.
        Supplier<Double> randomValue = () -> Math.random();

        // Print the random value using get()
        System.out.println("Get a random: " + randomValue.get());

        // Demo consumer
        Consumer<Double> printWithPrefixConsumer = d -> System.out.println("Value: " + d);
        printWithPrefixConsumer.accept(10.5);

        // Calc using lambda
        UnaryOperator<Integer> toSquareUnaryOperator = i -> i * i;
        System.out.println(toSquareUnaryOperator.apply(5));

        // Another example, with lambda brackets
        toSquareUnaryOperator = i -> {
            int result = i * i;
            System.out.println("Result: " + result);
            return result;
        };

        toSquareUnaryOperator.apply(4);

    }
}

