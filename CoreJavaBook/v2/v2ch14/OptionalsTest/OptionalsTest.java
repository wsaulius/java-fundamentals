import java.util.Optional;
import java.util.function.UnaryOperator;

class OptionalsTest {

    public static void main(String[] args) {

        Runnable myRunnable = () -> System.out.println("Running a runnable");
        myRunnable.run();

        final String stringVariable = "OneString";

        // of method does not allow null value
        Optional<String> stringOptional = Optional.of(stringVariable);

        System.out.println( stringOptional.get() );

        // ofNullable method does allow null value
        Optional<Integer> integerOptional = Optional.of( 12 );

        // Another example, with lambda brackets
        UnaryOperator<  Optional<Integer >> toSquareUnaryOperator = i -> {
            int result = i.get() * i.get();

            System.out.println("Result: " + result);
            return Optional.of( result );
        };

        toSquareUnaryOperator.apply((integerOptional.isPresent()) ? integerOptional :
                Optional.ofNullable(integerOptional.get()));

        integerOptional = Optional.ofNullable( null );

        try {

            toSquareUnaryOperator.apply((integerOptional.isPresent()) ? integerOptional :
                    Optional.ofNullable(integerOptional.get()));

        } catch ( java.util.NoSuchElementException ns ) {

            System.err.println( "Wow! it not a NP exception, but " + ns.getClass() );
            throw ns;
        }

    }
}

