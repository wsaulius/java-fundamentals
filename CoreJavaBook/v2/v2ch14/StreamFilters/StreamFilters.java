package StreamFilters;

import java.util.List;
import java.util.Arrays;
import java.util.Optional;
import java.util.OptionalDouble; 

import java.util.stream.Collectors; 

class StreamFilters {

    public static void main(String[] args) {

        List<String> names = Arrays.asList("Andrew", "Brandon", "Michael");
	    
        List<String> namesStartingWithA = names.stream()
                .filter(n -> n.startsWith("A"))
                .collect(Collectors.toList());

        namesStartingWithA.stream().forEach( System.out::println );
	    
        names = Arrays.asList("Andrew", "Brandon", "Michael");
	    
        List namesLengths = names.stream()
                .map(String::length)
                .collect(Collectors.toList());

        OptionalDouble averageNameLengthOptional = names.stream()
                .mapToInt(String::length)
                .average();
	    
        averageNameLengthOptional.ifPresent(System.out::println);

    }
}