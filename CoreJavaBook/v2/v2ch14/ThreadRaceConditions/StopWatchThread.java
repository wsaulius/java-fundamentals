package ThreadRaceConditions;

public class StopWatchThread extends Thread {

    private String threadName;

    StopWatchThread( final String threadName ) {
        this.threadName = threadName;
    }

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {

            System.out.println("Stop watch: [" + i + "] thread named " + this.threadName  );
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}